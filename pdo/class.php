<?php
  session_start();
  header("Content-Type: text/html; charset=utf-8");
  function message($text){
    exit('{"message" :"'.$text.'"}');
    };
  function go($url){
    exit('{"go" :"'.$url.'"}');
  };
  /**
   *
   */
  class Db
  {
    protected $db;
    function __construct()
    {
      $this->db = new PDO('mysql:host=127.0.0.1;dbname=investblog', 'admin', 'admin');
    }
    public function query($sql, $params = []) {
		$stmt = $this->db->prepare($sql);
		if (!empty($params)) {
			foreach ($params as $key => $val) {
				if (is_int($val)) {
					$type = PDO::PARAM_INT;
				} else {
					$type = PDO::PARAM_STR;
				}
				$stmt->bindValue(':'.$key, $val, $type);
			}
		}
		$stmt->execute();
		return $stmt;
	}
    public function row($sql,$params = [])
    {
      $result = $this->query($sql,$params);
      return $result->fetchAll(PDO::FETCH_ASSOC);
    }
    public function column($sql,$params = [])
    {
      $result = $this->query($sql,$params);
      return $result->fetchColumn();
    }

  //end class Db
  }
  //end class Db

  /**
   *
   */
  abstract class Model {
	  public $db;
  	public function __construct() {
  		$this->db = new Db;
  	}
  }
  class Main extends Model {
  	public function postsList()
    {
  		return $this->db->row('SELECT * FROM posts ORDER BY id DESC');
  	}
    public function exitSite()
    {
      session_destroy();
    }
    public function loginSite()
    {
      message('123');
    }




  }
?>
