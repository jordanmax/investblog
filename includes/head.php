<!DOCTYPE html>
<html lang="ru">
  <head>
    <!-- Meta Tag -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- SEO -->
    <meta name="description" content="150 words">
    <meta name="author" content="uipasta">
    <meta name="url" content="http://www.yourdomainname.com">
    <meta name="copyright" content="company name">
    <meta name="robots" content="index,follow">
    <title>StartInvest - учимся инвестировать</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" type="image/x-icon" href="images/favicon/apple-touch-icon.png">
    <!-- All CSS Plugins -->
    <link rel="stylesheet" type="text/css" href="css/plugin.css">
    <!-- Main CSS Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Google Web Fonts  -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
    <script src="js/main.js" charset="utf-8"></script>
    <!-- HTML5 shiv and Respond.js support IE8 or Older for HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	   <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
 <body>
	 <!-- Preloader Start -->
     <div class="preloader">
	   <div class="rounder"></div>
      </div>
      <!-- Preloader End -->
    <div id="main">
        <div class="container">
            <div class="row">
                 <!-- About Me (Left Sidebar) Start -->
                 <div class="col-md-3">
                   <div class="about-fixed">
                     <div class="my-pic">
                        <!--img src="images/pic/my-pic.png" alt="">
                        <a href="javascript:void(0)" class="collapsed" data-target="#menu" data-toggle="collapse"><i class="icon-menu menu"></i></a-->
                         <div id="menu" class="">
                           <ul class="menu-link">
                               <li><a href="about">О блоге</a></li>
                               <li><a href="work.html">Проекты</a></li>
                               <li><a href="contact.html">Контакты</a></li>
                            </ul>
                         </div>
                        </div>
                      <div class="my-detail">
                        <div class="white-spacing">
                            <h1>StartInvest</h1>
                            <span>Учимся инвестировать</span>
                        </div>
                       <!--ul class="social-icon">
                         <li><a href="#" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
                         <li><a href="#" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
                         <li><a href="#" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                         <li><a href="#" target="_blank" class="github"><i class="fa fa-github"></i></a></li>
                        </ul-->
                        <ul class="menu-link-new">
                          <li><a href="#" target="_blank" >Курс молодого инвестора</a></li>
                          <li><a href="#" target="_blank">Инвестируем вместе</a></li>
                          <li><a href="#" target="_blank">ТОП инвест проекты</a></li>
                          <li><a href="#" target="_blank">Последние СКАМЫ</a></li>
                        </ul>
                    </div>
                  </div>
                    <!-- Second block (Left Sidebar) End -->
                      <div class="col-md-14">
                        <div class="about-fixed">
                          <div class="my-pic">
                             <!--img src="images/pic/my-pic.png" alt="">
                             <a href="javascript:void(0)" class="collapsed" data-target="#menu" data-toggle="collapse"><i class="icon-menu menu"></i></a-->
                              <div id="menu" class="">
                                <ul class="menu-link">
                                    <li><a href="#" style="font-size: 16px;text-align: center;">Мой инвестиционный порртфель</a></li>
                                 </ul>
                              </div>
                             </div>
                           <div class="my-detail">
                             <ul class="menu-link-new">
                               <li><a href="https://btcrush.io/">BTCRUSH</a></li>
                               <li><a href="https://www.rediumtrade.com/">REDIUMTRADE</a></li>
                               <li><a href="https://profitablemorrows.com/">PROFITABLE MORROWS</a></li>
                               <li><a href="https://monopolist.biz/">MONOPOLIST</a></li>
                             </ul>
                         </div>
                       </div>
                      </div>
                    <!-- Second block (Left Sidebar) End -->
                </div>
                <!-- About Me (Left Sidebar) End -->
