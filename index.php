<?php include 'pdo/class.php';
      include 'includes/head.php';
      $pl = new Main;
?>
               <!-- Blog Post (Right Sidebar) Start -->
               <!--button onclick="post_query ('formhandler','login','')" class="btn btn-default">Добавити ще питання</button-->
   <div class="col-md-9">
      <div class="col-md-12 page-body">
      	<div class="row">
              <div class="sub-title">
             		<h2>Новости</h2>
                  <a href="contact.html"><i class="icon-envelope"></i></a>
               </div>
              <div class="col-md-12 content-page">
                  <!-- Blog Post Start -->
                  <div class="col-md-12 blog-post">
                      <div class="post-image">
                        <img src="images/blog/1.jpg" alt="">
                      </div>
                      <div class="post-title">
                        <a href="single.html"><h1>С чего начать свои путь молодого инвестора ? Учимся минимизировать риски.</h1></a>
                      </div>
                      <div class="post-info">
                      	<span>Май 15, 2018 / by <a href="#" target="_blank">InvestMan</a></span>
                      </div>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique temporibus tempora dicta soluta? Qui hic, voluptatem nemo quo corporis dignissimos voluptatum debitis cumque fugiat mollitia quasi quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique.</p>
                      <a href="single.html" class="button button-style button-anim fa fa-long-arrow-right"><span>Читать дальше</span></a>
                  </div>
                  <!-- Blog Post End -->
                  <!-- Blog Post Start -->
                  <div class="col-md-12 blog-post">
                      <div class="post-title">
                        <a href="single.html"><h1>How to design elegant e-mail newsletter in html for wish christmas to your subscribers?</h1></a>
                      </div>
                      <div class="post-info">
                      	<span>November 23, 2016 / by <a href="#" target="_blank">Alex Parker</a></span>
                      </div>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique temporibus tempora dicta soluta? Qui hic, voluptatem nemo quo corporis dignissimos voluptatum debitis cumque fugiat mollitia quasi quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique.</p>
                      <a href="single.html" class="button button-style button-anim fa fa-long-arrow-right"><span>Read More</span></a>
                  </div>
                  <!-- Blog Post End -->
                  <!-- Blog Post Start -->
                  <div class="col-md-12 blog-post">
                  	<div class="post-image">
                      	<img src="images/blog/1.jpg" alt="">
                      </div>
                      <div class="post-title">
                        <a href="single.html"><h1>Make mailchimp singup form working with ajax using jquery plugin</h1></a>
                      </div>
                      <div class="post-info">
                      	<span>November 23, 2016 / by <a href="#" target="_blank">Alex Parker</a></span>
                      </div>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique temporibus tempora dicta soluta? Qui hic, voluptatem nemo quo corporis dignissimos voluptatum debitis cumque fugiat mollitia quasi quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique.</p>
                      <a href="single.html" class="button button-style button-anim fa fa-long-arrow-right"><span>Read More</span></a>
                  </div>
                  <!-- Blog Post End -->
                  <!-- Blog Post Start -->
                  <div class="col-md-12 blog-post">
                      <div class="post-title">
                        <a href="single.html"><h1>Develop a custom wordpress membership plugin from scratch using framework on localhost - part 1</h1></a>
                      </div>
                      <div class="post-info">
                      	<span>November 23, 2016 / by <a href="#" target="_blank">Alex Parker</a></span>
                      </div>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique temporibus tempora dicta soluta? Qui hic, voluptatem nemo quo corporis dignissimos voluptatum debitis cumque fugiat mollitia quasi quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique.</p>
                      <a href="single.html" class="button button-style button-anim fa fa-long-arrow-right"><span>Read More</span></a>
                  </div>
                  <!-- Blog Post End -->
                  <!-- Blog Post Start -->
                  <div class="col-md-12 blog-post">
                      <div class="post-title">
                        <a href="single.html"><h1>How to customize a wordpress theme entirely from scratch using a child theme?</h1></a>
                      </div>
                      <div class="post-info">
                      	<span>November 23, 2016 / by <a href="#" target="_blank">Alex Parker</a></span>
                      </div>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique temporibus tempora dicta soluta? Qui hic, voluptatem nemo quo corporis dignissimos voluptatum debitis cumque fugiat mollitia quasi quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique.</p>
                      <a href="single.html" class="button button-style button-anim fa fa-long-arrow-right"><span>Read More</span></a>
                  </div>
                  <!-- Blog Post End -->
                  <!-- Blog Post Start -->
                  <div class="col-md-12 blog-post">
                  	<div class="post-image">
                      	<img src="images/blog/2.jpg" alt="">
                      </div>
                      <div class="post-title">
                        <a href="single.html"><h1>What makes wordpress a unique and highly customizable platform from competitors?</h1></a>
                      </div>
                      <div class="post-info">
                      	<span>November 23, 2016 / by <a href="#" target="_blank">Alex Parker</a></span>
                      </div>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique temporibus tempora dicta soluta? Qui hic, voluptatem nemo quo corporis dignissimos voluptatum debitis cumque fugiat mollitia quasi quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique.</p>
                      <a href="single.html" class="button button-style button-anim fa fa-long-arrow-right"><span>Read More</span></a>
                  </div>
                  <!-- Blog Post End -->
                  <!-- Blog Post Start -->
                  <div class="col-md-12 blog-post">
                      <div class="post-title">
                        <a href="single.html"><h1>What mistakes every beginner make, when they make website using bootstrap?</h1></a>
                      </div>
                      <div class="post-info">
                      	<span>November 23, 2016 / by <a href="#" target="_blank">Alex Parker</a></span>
                      </div>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique temporibus tempora dicta soluta? Qui hic, voluptatem nemo quo corporis dignissimos voluptatum debitis cumque fugiat mollitia quasi quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae ut ratione similique.</p>
                      <a href="single.html" class="button button-style button-anim fa fa-long-arrow-right"><span>Read More</span></a>
                  </div>
                  <!-- Blog Post End -->
                  <div class="col-md-12 text-center">
                   <a href="javascript:void(0)" id="load-more-post" class="load-more-button">Load</a>
                   <div id="post-end-message"></div>
                  </div>
               </div>
           </div>



          <!-- Subscribe Form Start -->
          <div class="col-md-8 col-md-offset-2">
             <form id="mc-form" method="post" action="http://uipasta.us14.list-manage.com/subscribe/post?u=854825d502cdc101233c08a21&amp;id=86e84d44b7">
               <div class="subscribe-form margin-top-20">
                <input id="mc-email" type="email" placeholder="Email" class="text-input">
                <button class="submit-btn" type="submit">Подписаться</button>
            	 </div>
               <p>Подпишись что бы получать уведомление о новых возможностях</p>
               <label for="mc-email" class="mc-label"></label>
              </form>
           </div>
             <!-- Subscribe Form End -->

      </div>


         <?php include 'includes/footer.php'; ?>


    </div>

                  <!-- Blog Post (Right Sidebar) End -->
<?php include 'includes/end.php'; ?>
